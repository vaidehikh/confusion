import React from 'react';
import { Card, CardImg, CardBody, CardGroup, BreadcrumbItem, Breadcrumb } from 'reactstrap';
import { Link } from 'react-router-dom';

function RenderDish({ dish }){
    return(
     
                    <Card >
                
                        <CardImg  object src={dish.image} alt={dish.name}/>
                        
                            <h4>{dish.name}</h4>
                        
                        <h4>{dish.description}</h4>
                    </Card>
                
    )

}
function RenderComments({ comments }) {
    console.log(comments)
    const commentsDisplay = comments.map((dishComments) => {
            
   return(
                    <div className="container">
                    
                    <CardBody>
                                <h4>{dishComments.comment}</h4>
                                <h4>--{dishComments.author},{" "}
                                
                                {new Intl.DateTimeFormat('en-US',{year: 'numeric',
                                 month: 'short', day: '2-digit' }).format(new Date(
                                Date.parse(dishComments.date)))}
                                </h4>
                            </CardBody>
                            </div>
                )
   }
  );
   return (
    <div className="container"> 
      
              {commentsDisplay}
       
    </div>
);
}
   
    const DishDetails = (props) =>  {
    if(props.dish != null){
       
        return(
         
            <div className="container">
                <div className='row'>
                    <Breadcrumb>
                       
                        <BreadcrumbItem>
                            <Link to = '/menu'>Menu</Link>
                        </BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className='col-12'>
                        <h3>{props.dish.name}</h3>
                        <hr/>
                    </div>
                </div> 
                 <CardGroup>
                     
               <RenderDish dish={props.dish}/>
               <Card>
               <h1> Comments</h1>
                <RenderComments comments={props.comments}/>
                </Card>
                </CardGroup>
                </div>
         
        )
        }
        else{
            return(
                <div></div>
            )
            
        }
    }



export default DishDetails;